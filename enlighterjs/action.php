<?php
/**
 * DokuWiki Plugin syntaxhighlighter4 (Action Component).
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  caar <caar>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
    die();
}

/**
   * Registers a callback function for a given event.
   *
   * @param Doku_Event_Handler $controller DokuWiki's event controller object
   *
   * @return void
   */

class action_plugin_enlighterjs extends DokuWiki_Action_Plugin {
/**
   * Registers a callback function for a given event.
   *
   * @param Doku_Event_Handler $controller DokuWiki's event controller object
   *
   * @return void
   */
  public function register(Doku_Event_Handler $controller) {
    $controller->register_hook('TPL_METAHEADER_OUTPUT', 'BEFORE', $this, 'handle_metaheader');
    $controller->register_hook('TPL_ACT_RENDER', 'AFTER', $this, 'handle_jsprocessing');
    $controller->register_hook('TOOLBAR_DEFINE', 'AFTER', $this, 'insert_button', array ());
  }

  public function insert_button(Doku_Event $event, $param) {
    $event->data[] = array (
        'type' => 'format',
        'title' => 'Test',
        'icon' => DOKU_BASE.'lib/plugins/enlighterjs/dist/img/tag.png',
        'open' => '<sxh>',
        'close' => '</sxh>',
        'block' => false,
    );
  }

  public function handle_metaheader(Doku_Event $event, $param) {
    $event->data['link'][] = array(
        'rel' => 'stylesheet',
        'type' => 'text/css',
        'href' => DOKU_BASE.'lib/plugins/enlighterjs/dist/EnlighterJS-3.4.0/enlighterjs.'.$this->getConf('theme').'.min.css',
    );

    $event->data['script'][] = array(
      'type' => 'text/javascript',
      'src' => DOKU_BASE.'lib/plugins/enlighterjs/dist/EnlighterJS-3.4.0/enlighterjs.min.js',
      '_data' => '',
    );
  }

  public function handle_jsprocessing(Doku_Event $event, $param) {
    global $ID, $INFO;

     // Ensures code will be written only on base page
     if ($ID != $INFO['id']) {
        return;
    }

    ptln('');
    ptln("<script type='text/javascript'>");
    ptln("EnlighterJS.init('code','pre', {");
    ptln("language : '".$this->getConf('lang')."',");
    ptln("theme: '".$this->getConf('theme')."',");
    ptln("indent: ".$this->getConf('indent').",");
    ptln("linehover: 1 === ".$this->getConf('linehover').",");
    ptln("linenumbers: 1 === ".$this->getConf('linenumbers'));
    ptln("});");
    ptln('</script>');
  }

}