<?php
/**
 * Default settings for the enlighterjs plugin.
 *
 * @author
 */

$conf['theme'] = 'classic';
$conf['lang'] = 'javascript';
$conf['indent'] = 2;
$conf['linehover'] = 0;
$conf['linenumbers'] = 1 ;
