<?php
/**
 * Options for the enlighterjs plugin.
 *
 * @author
 */

$meta['theme'] = array('multichoice', '_choices' => array(
  'atomic',
  'beyond',
  'bootstrap4',
  'classic',
  'dracula',
  'droide',
  'eclipse',
  'enlighter',
  'godzilla',
  'minimal',
  'monokai',
  'mowtwo',
  'rowhammer'
));

$meta['indent'] = array('numeric');

$meta['linehover'] = array('onoff');

$meta['linenumbers'] = array('onoff');

$meta['lang'] = array('multichoice', '_choices' => array(
  'abap',
  'apache',
  'asm',
  'avrassembly',
  'cpp',
  'csharp',
  'css',
  'cython',
  'cordpro',
  'diff',
  'dockerfile',
  'generic',
  'groovy',
  'go',
  'html',
  'ini',
  'conf',
  'java',
  'javascript',
  'json',
  'jsx',
  'kotlin',
  'latex',
  'less',
  'lighttpd',
  'lua',
  'mariadb',
  'markdown',
  'matlab',
  'mssql',
  'nginx',
  'nsis',
  'oracledb',
  'php',
  'powershell',
  'prolog',
  'python',
  'pb',
  'qml',
  'r',
  'raw',
  'routeros',
  'ruby',
  'rust',
  'scala',
  'scss',
  'shell',
  'sql',
  'squirrel',
  'swift',
  'typescript',
  'vhdl',
  'visualbasic',
  'verilog',
  'html',
  'yaml'
));