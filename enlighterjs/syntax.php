<?php
/**
 * DokuWiki Plugin syntaxhighlighter4 (Syntax Component).
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  CrazyMax <contact@crazymax.dev>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
  die();
}

if (!defined('DOKU_LF')) {
  define('DOKU_LF', "\n");
}
if (!defined('DOKU_TAB')) {
  define('DOKU_TAB', "\t");
}
if (!defined('DOKU_PLUGIN')) {
  define('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
}

require_once DOKU_PLUGIN.'syntax.php';

class syntax_plugin_enlighterjs extends DokuWiki_Syntax_Plugin {
  protected $syntax;

  public function getType() {
      return 'protected';
  }

  public function getPType() {
      return 'block';
  }

  /**
   * @return int Sort order - Low numbers go before high numbers
   */
  public function getSort() {
      return 194;
  }

  public function connectTo($mode) {
    $this->Lexer->addEntryPattern('<sxh(?=[^\r\n]*?>.*?</sxh>)', $mode, 'plugin_enlighterjs');
  }

  public function postConnect() {
    $this->Lexer->addExitPattern('</sxh>', 'plugin_enlighterjs');
  }

  public function handle($match, $state, $pos, Doku_Handler $handler) {
    switch ($state) {
        case DOKU_LEXER_ENTER:
            $this->syntax = strtolower(substr($match, 1));
            return false;
        case DOKU_LEXER_UNMATCHED:
            list($attr, $content) = preg_split('/>/u', $match, 2);
            if ($this->isSyntaxOk()) {
                $attr = trim($attr);
                if ($attr == null) {
                    $attr = $this->getConf('lang');
                }
            } else {
                $attr = null;
            }
            return array($this->syntax, $attr, $content);
    }
    return false;
  }

  public function render($mode, Doku_Renderer $renderer, $data) {
    if (count($data) != 3) {
        return true;
    }

    list($this->syntax, $attr, $content) = $data;

    if ($this->isSyntaxOk()) {
        $renderer->doc .=  '<div class="syntax"><code data-enlighter-language="'.strtolower($attr).'">'.htmlentities($content).'</code></div>';
    } else {
        $renderer->file($content);
    }
    return true;
  }

  private function isSyntaxOk() {
    if ($this->syntax == 'sxh') {
        return true;
    }
    return false;
  }

  function write_debug($what) {
    $handle = fopen('codeblock.txt', 'a');
    fwrite($handle,"$what\n");
    fclose($handle);
  }
}